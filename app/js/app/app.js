$(document).ready(function() {
	$('.selectpicker').selectpicker({
		size: 8
	});
});

$(document).ready(function(){
	
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})

$(document).ready(function(){
	
	$('ul.mini-tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.mini-tabs li').removeClass('current');
		$('.mini-tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})

$(document).ready(function(){

	$(".actions-toggle").hover(function () {
	    $(this).children(".actions").stop(true,true).delay(500).show(0);
	}, function () {
	    $(this).children(".actions").stop(true,true).delay(500).hide(0);
	});

	$(".actions").click( function(e) {
		e.stopPropagation();
	});

})